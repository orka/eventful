/**
 * Common error types definitions
 */
define(['p!-error'], function($error) {
    'use strict';

    var type = {
        NO_ERROR_LISTENER: 'NO_ERROR_LISTENER',
        EXCEPTION_WHILE_EMITING: 'EXCEPTION_WHILE_EMITING',
        ADD_LISTENER: 'ADD_LISTENER',
        UNREGISTERED: 'UNREGISTERED',
        PREVENTED: 'PREVENTED',
        MAX_HANDLER: 'MAX_HANDLER'
    };

    var ErrorClass = $error.subclass('EmitterError', {
        constructor: function(__type, __message) {
            ErrorClass.super.constructor.apply(this, arguments);
        },
        deconstructor: function() {
            ErrorClass.super.deconstructor.apply(this, arguments);
        },
        TYPE: type
    });

    ErrorClass.TYPE = type;
    return Object.freeze(ErrorClass);
});
