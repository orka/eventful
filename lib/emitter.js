/**
 * Siplme Class providing API to add/remove listeners
 * It is an abstract class and cares only that the types aredefined using String
 * More handling and memory managments can be done, but for now - its  pre-beta
 */
define(['p!-defineclass', 'p!-assertful', 'p!-eventful/error', 'p!-logger'], function($classify, $assertful, $error, $logger) {
    'use strict';
    var EmitterClass;
    var MAX_HANDLERS_PER_TYPE = 101;

    function setMax(__pint) {
        if ($assertful.pint(__pint)) {
            MAX_HANDLERS_PER_TYPE = __pint;
        } else {
            throw $error($error.TYPE.TYPE_ERROR, 'Unable to set MAX_HANDLERS_PER_TYPE').log();
        }
    }

    EmitterClass = $classify('Emitter', {
        /**
         * Creates new collection of handlers/[types]
         * @param   {Array} __restrictedEventTypesArray Opt restrict instance to enet type specified in array
         */
        constructor: function(__restrictedEventTypesArray) {
            if (!$assertful.optNearray(__restrictedEventTypesArray)) {
                $error($error.TYPE.TYPE_ERROR, 'invalid arguments provided, only arrays').throw();
            }
            this.__restrictedEventTypesArray = this.__restrictedEventTypesArray ?
                this.__restrictedEventTypesArray.concat(__restrictedEventTypesArray) :
                __restrictedEventTypesArray;
            this.__handlersCollection__ = this.__handlersCollection__ || {};
        },

        /**
         * Derefrences all objects assocciated with this class
         */
        deconstructor: function() {
            this.__handlersCollection__ = null;
            this.__restrictedEventTypesArray = null;
        },

        /**
         * Validating Type to be a String, start with lower case and be more than 1 character
         * If any of the tests fail - attempts to emit "error" with related message
         * @param   {String} __type Event type to be validated
         * @returns {Boolean}        true if validation succeeded, else fasle
         */
        __isValidType: function(__type) {
            if (!$assertful.flostr(__type) || __type.length < 3) {
                return false;
            }

            return true;
        },

        /**
         * Validating Handler to be a Function
         * If test fails - attempts to emit "error" with related message
         * @param   {Function} __handler Event type to be validated
         * @returns {Boolean}  true if validation succeeded, else fasle
         */
        __isValidHandler: function(__handler) {
            if (!$assertful.func(__handler)) {
                return false;
            }
            return true;
        },

        verbose: function() {
            return;
            $logger.verbose.apply($logger, [this.classname].concat(Array.prototype.slice.call(arguments, 0)).concat([$logger.TAGS.GENERAL]));
        },

        /**
         * Registers handler to be executen when "type" event is emitted
         * If wrong arguments provided or a restriction exists and the type vialates it the "error" event is emitted
         * @param {String} __type Event type (name) unique
         * @param {Function} __handler Handler
         * @returns {Boolean} true if regitered a new event, false if already exists or error had occured
         */
        addListener: function(__type, __handler) {
            var _stash;

            if (!this.__isValidType(__type)) {
                $error($error.TYPE.TYPE_ERROR, 'Type myst be a String with a first lowercase: ' + __type).throw();
            }

            if (!this.__isValidHandler(__handler)) {
                $error($error.TYPE.TYPE_ERROR, 'No handler was specified: ' + __type).throw();
            }

            if (this.__restrictedEventTypesArray && this.__restrictedEventTypesArray.indexOf(__type) < 0) {
                $error($error.TYPE.PREVENTED, 'Not allowed to register event type: ' + __type).throw();
            }

            _stash = this.__handlersCollection__[__type];

            if (_stash && _stash.length >= MAX_HANDLERS_PER_TYPE) {
                // return false;
                $error($error.TYPE.MAX_HANDLER, 'Max handlers reached (MAX_HANDLERS_PER_TYPEL: ' + MAX_HANDLERS_PER_TYPE + '), you may have a memory leack, else increace the maximum!').throw();
            }

            if (!_stash) {
                _stash = this.__handlersCollection__[__type] = [];
            } else if (_stash.indexOf(__handler) >= 0) {
                //if event already registerred
                this.verbose('Listener already exist');
                return false;
            }
            //register new event handler
            _stash.push(__handler);

            return true;
        },

        /*addListeners: function(__typeArray, __handler) {
            var _i;
            if (!$assertful.array(__typeArray)) {
                $error($error.TYPE.TYPE_ERROR, 'Array of type is expected').throw();
            }

            for (_i = 0; _i < __typeArray.length; _i += 1) {
                this.addListener(__typeArray[_i], __handler.bind(null));
            }
        },
*/
        /**
         * Counts handlers per specified event type
         * @param   {String} __type Event name
         * @returns {Number}  number of handlers attached to specified event
         */
        getTypeCount: function(__type) {
            var _stash = this.__handlersCollection__[__type];
            return _stash ? _stash.length : 0;
        },

        /**
         * Deleted handler refrence in Event "type" collections
         * !Important - this method will not throw if wrong arguments provided as the function references could have been deleted
         * @param   {String} __type    Event type (name)
         * @param   {Function} __handler Handler !IMPORTANT - handler must be the same function as when you regirtered it in the first place
         * @returns {Boolean} true if found and removed - else - either not found or aruments are wrong
         */
        removeListener: function(__type, __handler) {
            var _stash, _handlerIndex;

            if (!this.__isValidType(__type)) {
                return false;
            }

            if (!this.__isValidHandler(__handler)) {
                return false;
            }

            if (!$assertful.object(this.__handlersCollection__)) {
                return false;
            }
            //access stash
            _stash = this.__handlersCollection__[__type];
            //if not defined - means it was never created
            if (!$assertful.nearray(_stash)) {
                //silently fail
                this.verbose('no type was found');
                //no more type found - delete the object refference
                delete this.__handlersCollection__[__type];
                return false;
            }
            //find handler
            _handlerIndex = _stash.indexOf(__handler);
            //if found - remove from array with splice
            if (_handlerIndex >= 0) {
                //aplice out handler
                _stash.splice(_handlerIndex, 1);
                this.verbose('Listener removed: ' + __type);
                //if no more type found - delete the object refference
                if (_stash.length === 0) {
                    delete this.__handlersCollection__[__type];
                }
                return true;
            }

            //if no more type found - delete the object refference
            if (_stash.length === 0) {
                delete this.__handlersCollection__[__type];
            }
            return false;
        },

        /**
         * Removed (derefferences) all handlers assocciated with Event "type"
         * @param   {String} __type Event Type target
         * @returns {Boolean}  true if found and removed - else - either not found or aruments are wrong
         */
        removeTypeListeners: function(__type) {
            var _stash;

            if (!this.__isValidType(__type)) {
                return false;
            }

            //access stash
            _stash = this.__handlersCollection__[__type];
            //if defined - clear it!
            if (_stash) {
                delete this.__handlersCollection__[__type];
                this.verbose('Clears all types: ' + __type);
            }

            return true;
        },

        /**
         * Simply dereferences all associated types by assigning new Object as a collection
         */
        removeAllListeners: function() {
            //this will siply derefrence all the listeners and types
            this.__handlersCollection__ = {};
            this.verbose('All listeners cleared');
        },

        /**
         * Test if Event type was registered
         * @param   {String}  __type event to be found
         * @returns {Boolean} true if found, else false
         */
        hasEvent: function(__type) {
            if (!$assertful.string(__type)) {
                return false;
            }

            if (this.__handlersCollection__ && this.__handlersCollection__[__type]) {
                return true;
            }

            return false;
        },

        /**
         * Finds all handlers of "type" in a collection and fires handlers one by one
         * If handler returns false the the loop is broken out of and all other handlers assocciated with type that were not yet executed - will not be executed
         * If a handler throws exception - it is
         * @param   {[type]} __type    [description]
         * @param   {[type]} __options [description]
         * @returns {[type]}           [description]
         */
        emit: function(__type) {
            var _stash, _i, _handler, _stashCopy;
            if (!this.__isValidType(__type)) {
                throw $error($error.TYPE.TYPE_ERROR, 'Type must be a String with a first lowercase: ' + __type, arguments);
            }

            if (!this.__handlersCollection__) {
                this.verbose('Object was already deconstructed: ' + __type);
                return false;
            }

            //find stash
            _stash = this.__handlersCollection__[__type];

            //do nothing if the event was never registered
            if (!$assertful.array(_stash) || $assertful.earray(_stash)) {
                //delete empty reference
                delete this.__handlersCollection__[__type];
                this.verbose('Nothing to emit: ' + __type);

                //only if the type is "error" throw the exception up the stack
                if (__type === 'error') {
                    $error($error.TYPE.UNREGISTERED, '"' + __type + '" Event was never registered', arguments).throw();
                }
                return false;
            }

            //clone array as the state may change durring propagation
            _stashCopy = _stash.slice(0);

            //loop over all t
            for (_i = 0; _i < _stashCopy.length; _i += 1) {
                _handler = _stashCopy[_i];
                try {
                    //Do not apply 'THIS'
                    _handler(this, arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6]);
                } catch (__err) {
                    // _nerr =  $error($error.TYPE.EXCEPTION_WHILE_EMITING, 'Caught exception while emitting ' + __type + ' event!', __type, __err);
                    //if the error was thrown while processing 'error' event - there in nothing we can do - throw the error up the stack
                    if (__type === 'error' || !this.__handlersCollection__['error']) {
                        throw __err;
                    }

                    //try and emit error event
                    this.emit('error', __err);
                }
            }
            return true;
        },

        emitUntil: function(__breakOnThis, __type) {
            var _stash, _i, _handler, _ret;

            if (!this.__isValidType(__type)) {
                return false;
            }

            if (__breakOnThis === undefined) {
                throw 'Must specify return to break on';
            }

            //find stash
            _stash = this.__handlersCollection__[__type];

            //do nothing if the event was never registered
            if (!$assertful.nearray(_stash)) {
                //delete empty reference
                delete this.__handlersCollection__[__type];
                this.verbose('Nothing to emit: ' + __type);

                //only if the type is "error" throw the exception up the stack
                if (__type === 'error') {
                    throw 'Event was never registered';
                }

                //else emit error
                this.emit('error', $error($error.INVALID_ARGUMENTS, 'No listener of type "' + __type + '" was found!'));

                return false;
            }

            //loop over all t
            for (_i = 0; _i < _stash.length; _i += 1) {
                _handler = _stash[_i];
                try {
                    if (_handler(arguments[2], arguments[3], arguments[4], arguments[5], arguments[6]) === __breakOnThis) {
                        return true;
                    }
                } catch (__err) {
                    //if the error was thrown while processinsg 'error' event - there in nothing we can do - throw the error up the stack
                    if (__type === 'error') {
                        throw __err;
                    }

                    //try and emit error event
                    _ret = this.emit('error', $error($error.EXCEPTION, 'Caught exception while emitting event', __err));

                    if (_ret === false) {
                        //if no event handler was registerred (false) - throw Error up the stack
                        throw __err;
                    }
                }
            }
            return false;
        }
    });

    EmitterClass.setMax = setMax;
    EmitterClass.error = $error;
    EmitterClass.MAX_HANDLERS_PER_TYPE = MAX_HANDLERS_PER_TYPE;
    return EmitterClass;
});
