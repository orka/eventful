define(['lib/emitter', 'p!-defineclass'], function($Emitter, $clacify) {
    'use strict';
    describe('$Emitter', function() {
        it('should be a funacion', function() {
            expect($Emitter).toBeDefined();
            expect(typeof $Emitter).toBe('function');
        });
        it('should be named "Emitter"', function() {
            expect($Emitter.name).toEqual('Emitter');
        });
        it('should be $clacified', function() {
            expect($clacify.isClass($Emitter)).toBe(true);
        });
        it('should be able to be subclassed', function() {
            var _cl = $Emitter.subclass('NewEmitterType', {
                constructor: function() {},
                deconstructor: function() {}
            });
            expect($clacify.isClass(_cl)).toBe(true);
        });
        it('should be able retain super chain', function() {
            var _cl = $Emitter.subclass('NewEmitterType', {
                constructor: function() {},
                deconstructor: function() {}
            });
            expect($clacify.isClass(_cl.super)).toBe(true);
            expect($clacify.isClass(_cl.super === $Emitter)).toBe(true);
        });
        it('should create a proper instance', function() {
            var _emitter = $Emitter();
            expect(_emitter.instanceof($Emitter)).toBe(true);
        });
    });

    describe('$Emitter addListener', function() {
        var _instance;
        beforeEach(function() {
            _instance = $Emitter();
        });

        function dummyHandler() {

        }

        function dummyHandler2() {

        }

        it('should not throw if a valid with args (string>3, function)', function() {
            expect(_instance.addListener.bind(_instance, 'err', dummyHandler)).not.toThrow();
        });

        it('should throw if type is String < 3 || not lower case first letter', function() {
            expect(_instance.addListener.bind(_instance, '', dummyHandler)).toThrow();
            expect(_instance.addListener.bind(_instance, 'ee', dummyHandler)).toThrow();
            expect(_instance.addListener.bind(_instance, 'Error', dummyHandler)).toThrow();
        });

        it('should throw if type is null|undefined', function() {
            expect(_instance.addListener.bind(_instance, undefined, dummyHandler)).toThrow();
            expect(_instance.addListener.bind(_instance, null, dummyHandler)).toThrow();
        });

        it('should throw if type is Boolean', function() {
            expect(_instance.addListener.bind(_instance, true, dummyHandler)).toThrow();
            expect(_instance.addListener.bind(_instance, false, dummyHandler)).toThrow();
        });

        it('should throw if type is Function', function() {
            expect(_instance.addListener.bind(_instance, dummyHandler, dummyHandler)).toThrow();
        });

        it('should throw if type is Object []{}', function() {
            expect(_instance.addListener.bind(_instance, {}, dummyHandler)).toThrow();
            expect(_instance.addListener.bind(_instance, [], dummyHandler)).toThrow();
        });

        it('should throw if type is Number or Float Infinity (+-)', function() {
            expect(_instance.addListener.bind(_instance, 0, dummyHandler)).toThrow();
            expect(_instance.addListener.bind(_instance, 1, dummyHandler)).toThrow();
            expect(_instance.addListener.bind(_instance, 555, dummyHandler)).toThrow();
            expect(_instance.addListener.bind(_instance, -666, dummyHandler)).toThrow();
            expect(_instance.addListener.bind(_instance, Infinity, dummyHandler)).toThrow();
            expect(_instance.addListener.bind(_instance, -Infinity, dummyHandler)).toThrow();
            expect(_instance.addListener.bind(_instance, 0.01, dummyHandler)).toThrow();
            expect(_instance.addListener.bind(_instance, -0.01, dummyHandler)).toThrow();
        });

        it('should "throw" if handler in not provided or not a function', function() {
            expect(_instance.addListener.bind(_instance, 'error', null)).toThrow();
            expect(_instance.addListener.bind(_instance, 'error', true)).toThrow();
            expect(_instance.addListener.bind(_instance, 'error', false)).toThrow();
            expect(_instance.addListener.bind(_instance, 'error', [])).toThrow();
            expect(_instance.addListener.bind(_instance, 'error', {})).toThrow();
            expect(_instance.addListener.bind(_instance, 'error', 123)).toThrow();
            expect(_instance.addListener.bind(_instance, 'error', 'error')).toThrow();
        });

        it('should register event', function() {
            expect(_instance.addListener.bind(_instance, 'error', dummyHandler)).not.toThrow();
            expect(_instance.hasEvent('error')).toBe(true);
        });

        it('should not "throw" if the same type/handler added', function() {
            expect(_instance.addListener.bind(_instance, 'error', dummyHandler)).not.toThrow();
            expect(_instance.addListener.bind(_instance, 'error', dummyHandler)).not.toThrow();
        });

        it('should return "true" if new type/handler added', function() {
            expect(_instance.addListener('error', dummyHandler)).toBe(true);
            expect(_instance.addListener('error', dummyHandler)).toBe(false);
        });

        it('should not "throw" if the same type but different hadnler added', function() {
            expect(_instance.addListener.bind(_instance, 'error', dummyHandler)).not.toThrow();
            expect(_instance.addListener.bind(_instance, 'error', dummyHandler2)).not.toThrow();
        });

        it('should return "true" if same type but different handler added', function() {
            expect(_instance.addListener('error', dummyHandler)).toBe(true);
            expect(_instance.addListener('error', dummyHandler2)).toBe(true);
        });

        it('should not register same type, same handler', function() {
            expect(_instance.addListener('error', dummyHandler)).toBe(true);
            expect(_instance.addListener('error', dummyHandler)).toBe(false);
        });

        it('should throw if max handlers added', function() {
            $Emitter.setMax(1);
            _instance.addListener('error', dummyHandler);
            expect(_instance.addListener.bind(_instance, 'error', dummyHandler2)).toThrow();
            $Emitter.setMax(10);
        });
    });

    describe('$Emitter removeListener', function() {
        var _instance;
        beforeEach(function() {
            _instance = $Emitter();
        });

        function dummyHandler() {

        }

        function dummyHandler2() {

        }

        it('does not throw', function() {
            _instance.addListener('test', dummyHandler);
            expect(_instance.removeListener.bind(_instance, 'test', dummyHandler)).not.toThrow();
            expect(_instance.removeListener.bind(_instance, '', dummyHandler)).not.toThrow();
            expect(_instance.removeListener.bind(_instance, 'test')).not.toThrow();
            expect(_instance.removeListener.bind(_instance)).not.toThrow();
        });

        it('removed only targeted listener', function() {
            _instance.addListener('test', dummyHandler);
            _instance.addListener('test', dummyHandler2);
            expect(_instance.getTypeCount('test')).toEqual(2);
            _instance.removeListener('test', dummyHandler);
            expect(_instance.getTypeCount('test')).toEqual(1);
        });
    });

    describe('$Emitter removeTypeListeners', function() {
        var _instance;
        beforeEach(function() {
            _instance = $Emitter();
        });

        function dummyHandler() {

        }

        function dummyHandler2() {

        }

        it('does not throw', function() {
            _instance.addListener('test', dummyHandler);
            expect(_instance.removeListener.bind(_instance, 'test')).not.toThrow();
            expect(_instance.removeListener.bind(_instance, '')).not.toThrow();
            expect(_instance.removeListener.bind(_instance, 'asd')).not.toThrow();
            expect(_instance.removeListener.bind(_instance), []).not.toThrow();
            expect(_instance.removeListener.bind(_instance)).not.toThrow();
        });

        it('removes all "type" listeners', function() {
            _instance.addListener('test', dummyHandler);
            _instance.addListener('test', dummyHandler2);
            expect(_instance.getTypeCount('test')).toEqual(2);
            expect(_instance.removeTypeListeners.bind(_instance, 'test')).not.toThrow();
            expect(_instance.getTypeCount('test')).toEqual(0);
        });
    });

    describe('$Emitter emit', function() {
        var _instance;
        var _called = false;
        var _called2 = false;
        beforeEach(function() {
            _instance = $Emitter();
            _called = false;
            _called2 = false;
        });

        function dummyHandler() {
            _called = true;
        }

        function dummyHandler2() {
            _called2 = true;
        }

        it('should "throw" if emit is called without valid type', function() {
            expect(_instance.emit.bind(_instance, null)).toThrow();
            expect(_instance.emit.bind(_instance, true)).toThrow();
            expect(_instance.emit.bind(_instance, false)).toThrow();
            expect(_instance.emit.bind(_instance, undefined)).toThrow();
            expect(_instance.emit.bind(_instance)).toThrow();
            expect(_instance.emit.bind(_instance, {})).toThrow();
            expect(_instance.emit.bind(_instance, [])).toThrow();
            expect(_instance.emit.bind(_instance, 123)).toThrow();
        });

        it('should NOT "throw" if emit is called on non existing type (except "error")', function() {
            expect(_instance.emit.bind(_instance, 'someEvent', dummyHandler)).not.toThrow();
        });

        it('should "throw" if emit is called on non existing type for "error"', function() {
            expect(_instance.emit.bind(_instance, 'error', dummyHandler)).toThrow();
        });

        it('should return false if no listener was found', function() {
            expect(_instance.emit.bind(_instance, 'someEvent', dummyHandler)()).toEqual(false);
        });

        it('should return true if listener was found', function() {
            _instance.addListener('error', dummyHandler);
            expect(_instance.emit.bind(_instance, 'error', dummyHandler)()).toEqual(true);
        });

        it('should call all handlers on a registerred event with multiple handlers', function() {
            _instance.addListener('error', dummyHandler);
            _instance.addListener('error', dummyHandler2);
            expect(_instance.emit.bind(_instance, 'error')).not.toThrow();
            expect(_called === _called2 === true).toBe(true);
        });

        it('should emit "error" if one or more handlers "throws"', function() {
            _instance.addListener('error', dummyHandler);
            _instance.addListener('test', function() {
                throw '';
            });
            _instance.addListener('test', dummyHandler2);

            expect(_instance.emit.bind(_instance, 'test')).not.toThrow();

            expect(_called === _called2 === true).toBe(true);
        });

        it('should "throw" if one or more handlers "throws" and no error handlers defined', function() {
            _instance.addListener('test', function() {
                throw '';
            });
            expect(_instance.emit.bind(_instance, 'test')).toThrow();
        });
    });
});
