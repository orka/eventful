this.$PROJECT_NAME = 'eventful';

require.config({
    map: {
        '*': {
            p: 'packages/requirejs/src/package',
            json: 'packages/requirejs/src/json',
            text: 'packages/requirejs/src/text'
        }
    },
    paths: {
        lib: '../lib',
        packages: '../packages'
    }
});

/**
 * Spec must be wrapped in a define function
 * then be followed by a window.onload() to trigger jasmine tests
 * window.onload() is a hack around jasmin not being triggerred when using requirejs
 * firt args array is the specs to be executed
 */
define(['test'], function() {
    'use strict';
    window.onload();
});
