define(['p!-eventful/emitter', 'p!-eventful/error'], function ($emitter, $error) {
    'use strict';
    return {
        emitter: $emitter,
        error: $error
    };
});
